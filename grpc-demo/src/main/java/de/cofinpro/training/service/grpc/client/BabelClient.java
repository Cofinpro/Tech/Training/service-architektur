package de.cofinpro.training.service.grpc.client;

import de.cofinpro.training.service.grpc.BabelGrpc;
import de.cofinpro.training.service.grpc.Text;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class BabelClient implements AutoCloseable{
    private final ManagedChannel channel;
    private final BabelGrpc.BabelBlockingStub blockingStub;
    private final BabelGrpc.BabelStub asyncStub;

    BabelClient(String host, int port) {
        this(ManagedChannelBuilder.forAddress(host, port).usePlaintext().build());
    }

    BabelClient(ManagedChannel channelBuilder) {
        channel = channelBuilder;
        blockingStub = BabelGrpc.newBlockingStub(channel);
        asyncStub = BabelGrpc.newStub(channel);
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        if (args.length != 1) {
            System.err.println("Usage: please pass a single word to translate");
        }

        try(BabelClient client = new BabelClient("localhost", 8080)) {
            // async
            client.translate(args[0], System.out::println).get();

            // blocking
            client.translate(args[0]).forEach(System.out::println);
        }

    }

    List<String> translate(String text) {
        final Iterator<Text> translation = blockingStub.translate(Text.newBuilder().setText(text).build());
        return StreamSupport.stream(Spliterators.spliteratorUnknownSize(translation, Spliterator.IMMUTABLE), false)
                .map(Text::getText)
                .collect(Collectors.toList());
    }

    CompletableFuture<Void> translate(String text, Consumer<String> callback) {
        final CompletableFuture<Void> completableFuture = new CompletableFuture<>();
        asyncStub.translate(Text.newBuilder().setText(text).build(), new StreamObserver<>() {
            @Override
            public void onNext(Text text) {
                callback.accept(text.getText());
            }

            @Override
            public void onError(Throwable throwable) {
                completableFuture.completeExceptionally(throwable);
            }

            @Override
            public void onCompleted() {
                completableFuture.complete(null);
            }
        });
        return completableFuture;
    }


    @Override
    public void close() {
        channel.shutdown();
    }
}
