package de.cofinpro.training.service.grpc.server;



import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class DictionaryTest {

    @Test
    void entryStream() throws IOException {
        final long count = new Dictionary().entryStream().limit(100).count();
        assertEquals(100, count);
    }
}
