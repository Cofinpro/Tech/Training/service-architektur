package de.cofinpro.training.service.jaxws;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class Client {

    private final Babel babel;

    private Client() {
        this.babel = new BabelService().getBabel();
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: please pass a single word to translate");
        }
        final String translation = new Client().translate(args[0]);
        System.out.println(translation);
    }

    private String translate(String text) {
        return babel.translate("de_en", text);
    }
}
