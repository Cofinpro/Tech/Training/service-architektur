package de.cofinpro.training.service.jaxws;

import java.io.*;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class Dictionary {

    private byte[] dict;

    Dictionary() {
        try(var stream = getClass().getClassLoader().getResourceAsStream("de-en.txt")) {
            if (stream == null) {
                throw new FileNotFoundException("Could not find dictionary file de-en.txt");
            }
            this.dict = stream.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Optional<String> lookup(String text) {
        try (var in = new ByteArrayInputStream(dict);
             var reader = new InputStreamReader(in);
             var buffer = new BufferedReader(reader)) {

            while (true) {
                // skip the header
                if (!buffer.readLine().startsWith("#")) break;
            }

            final var firstWordPattern = Pattern.compile("([\\w\\s]+).*");
            final Matcher matcher = firstWordPattern.matcher("");
            String line;

            while ((line = buffer.readLine()) != null) {
                if (isAMatch(text, line, matcher)) {
                    final String[] entry = line.split("::");
                    return Optional.of(entry[1].trim());
                }
                // Not there yet - keep looping
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return Optional.empty();
    }

    private boolean isAMatch(String text, String entry, Matcher matcher) {
        matcher.reset(entry);
        if (!matcher.matches()) {
            // cannot parse
            return false;
        }
        final String firstWord = matcher.group(1);
        return firstWord.equalsIgnoreCase(text);
    }
}
