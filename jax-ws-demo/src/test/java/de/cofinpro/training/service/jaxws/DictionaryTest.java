package de.cofinpro.training.service.jaxws;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class DictionaryTest {

    private Dictionary dict = new Dictionary();

    @Test
    void lookup() throws IOException {
        final Optional<String> translation = dict.lookup("Hallo");
        assertTrue(translation.isPresent());
        assertEquals("Hi!; Hi there!", translation.get());
    }
}