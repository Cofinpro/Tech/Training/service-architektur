package de.cofinpro.training.service.jaxws;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceRef;
import javax.xml.ws.soap.SOAPBinding;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
public class BabelServiceClient {

    public static void main(String[] args) throws Exception {
        BabelServiceClient client = new BabelServiceClient();
        final String translation = client.translate("Hallo");
        System.out.println(translation);
    }

    private String translate(String text) throws MalformedURLException {
        final URL endpoint = new URL("http://localhost:8080/BabelService?wsdl");
        QName serviceName = new QName("http://jaxws.service.training.cofinpro.de/", "BabelService");
        final Service babelService = Service.create(endpoint, serviceName);

        final Babel babel = babelService.getPort(Babel.class);
        return babel.translate("de_en", "Hallo");
    }


}
