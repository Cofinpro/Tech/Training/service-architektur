# GraphQL Demo

Ein einfacher Demo-Server für Graph. Er setzt auf [GraphQL Java](https://www.graphql-java.com) auf.

## Learn

[graphql.org](https://graphql.org)

## Run

Am einfachsten lässt sich der Server per MVN starten:

```bash
mvn exec:java
```

Danach hört her auf [http://localhost:8080/](http://localhost:8080/)

## Interacting

Der Server akzeptiert Queries via GET-Request:
```bash
$ curl -g 'http://localhost:8080/graphql?query={bookById(id:"book-1"){name}}'
{"data":{"bookById":{"name":"Harry Potter and the Philosopher's Stone"}}}%
```

Das lässt sich auch über das Webinterface (siehe Link oben) ansprechen.

Normalerweise werden Queries jedoch über POST als JSON geschickt:

```bash
$ curl 'http://localhost:8080/graphql' -H "Content-Type: application/json" -d '{"query":"{bookById(id:\"book-1\"){ name }}"}'
{"data":{"bookById":{"name":"Harry Potter and the Philosopher's Stone"}}}%
```

Auch die Kurzform geht:

```bash
$ curl 'http://localhost:8080/graphql' -H "Content-Type: application/graphql" -d '{bookById(id:"book-1"){name}}'
{"data":{"bookById":{"name":"Harry Potter and the Philosopher's Stone"}}}%
```


```bash
$ http 'http://localhost:8080/graphql' query="{__schema { queryType { fields { name }} }}"
HTTP/1.1 200 OK
Connection: keep-alive
Content-Length: 68
Content-Type: application/json
Date: Mon, 12 Aug 2019 05:41:40 GMT
```
```json
{
    "data": {
        "__schema": {
            "queryType": {
                "fields": [
                    {
                        "name": "bookById"
                    }
                ]
            }
        }
    }
}
```