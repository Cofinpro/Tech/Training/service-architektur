package de.cofinpro.training.service.jaxws;

import graphql.schema.DataFetcher;
import graphql.schema.idl.RuntimeWiring;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class GraphQLDataFetchers {

    static RuntimeWiring buildRuntimeWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .type(newTypeWiring("Query")
                        .dataFetcher("bookById", getBookByIdDataFetcher())
                ).build();
    }

    private static List<Map<String, String>> books = Arrays.asList(
            Map.of("id", "book-1",
                    "name", "Harry Potter and the Philosopher's Stone",
                    "pageCount", "223",
                    "authorId", "author-1"),
            Map.of("id", "book-2",
                    "name", "Moby Dick",
                    "pageCount", "635",
                    "authorId", "author-2"),
            Map.of("id", "book-3",
                    "name", "Interview with the vampire",
                    "pageCount", "371",
                    "authorId", "author-3")
    );

    private static List<Map<String, String>> authors = Arrays.asList(
            Map.of("id", "author-1",
                    "firstName", "Joanne",
                    "lastName", "Rowling"),
            Map.of("id", "author-2",
                    "firstName", "Herman",
                    "lastName", "Melville"),
            Map.of("id", "author-3",
                    "firstName", "Anne",
                    "lastName", "Rice")
    );

    private static DataFetcher getBookByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            String bookId = dataFetchingEnvironment.getArgument("id");

            if (bookId != null) {
                return books
                        .stream()
                        .filter(book -> book.get("id").equals(bookId))
                        .findFirst()
                        .orElse(null);
            } else {
                return books;
            }
        };
    }
}
