package de.cofinpro.training.service.jaxws;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Gregor Tudan, Cofinpro AG
 */
class GraphQLDataFetchersTest {

    private GraphQL graphQL;

    @BeforeEach
    void setUp() {
        final InputStream schemaFile = getClass().getClassLoader().getResourceAsStream("schema.graphql");
        final TypeDefinitionRegistry registry = new SchemaParser().parse(new InputStreamReader(schemaFile));
        final RuntimeWiring wiring = GraphQLDataFetchers.buildRuntimeWiring();
        final GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(registry, wiring);
        this.graphQL = GraphQL.newGraphQL(schema).build();
    }

    @Test
    void testGetBookById() {
        final ExecutionResult result = graphQL.execute("{ bookById(id: \"book-1\") { name }}");
        assertTrue(result.isDataPresent());
        assertTrue(result.getErrors().isEmpty());
    }
}